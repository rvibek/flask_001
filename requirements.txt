Flask==1.0.3
frozen-flask
Flask-WTF==0.14.2
Jinja2==2.10.1
MarkupSafe==1.1.1
pycparser==2.19
python-dotenv==0.10.3
frozen-flask